﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;


public class playerscript : MonoBehaviour
{
    Rigidbody playerRigidbody;
    Vector3 movement;
    [SerializeField]
    float SPEED = 5.0f;
    [SerializeField]
    Animator anim;
    bool isMoving = false;
    [SerializeField]
    int floorMask;
    [SerializeField]
    RaycastHit hit;
    [SerializeField]
    Ray CamRay;
    [SerializeField]
    Camera camera;
    [SerializeField]
    Ray player_ray;

    [SerializeField]
    RaycastHit player_hit;
    [SerializeField]
    Transform center;

    float RAY_LENGTH = 1000.0f;

    void Awake()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        floorMask = LayerMask.GetMask("floor");
    }
    // Use this for initialization
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");
        Move(h, v);
        Debug.Log(h + "::" + v);
        if (!Mathf.Approximately(v, 0.0f) || !Mathf.Approximately(h, 0.0f))
        {
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }

        MakeAnimation();
        MouseLook();


    }
    void Move(float h, float v)
    {
        movement.Set(h, 0.0f, v);
        movement = movement.normalized * Time.deltaTime * SPEED;
        playerRigidbody.MovePosition(transform.position + movement);
    }

    void MakeAnimation()
    {
        if(isMoving)
        {

            Debug.Log("Inside is moving 1");
            anim.SetFloat("isMoving", 1.0f);
        }
        else
        {
            Debug.Log("Inside is moving 2");

            anim.SetFloat("isMoving", 0.0f);
        }
    }
    void MouseLook()
    {
        CamRay = camera.ScreenPointToRay(Input.mousePosition);
        Debug.DrawRay(CamRay.origin, CamRay.direction*1000, Color.red);

        if (Physics.Raycast(CamRay, out hit, RAY_LENGTH, floorMask ))
        {
            Transform objectHit = hit.transform;
          //  Vector3 Camera_to_Mouse = hit.point - transform.position;

            Vector3 Pointer_to_Mouse = hit.point - transform.position;
            Debug.Log("Player center::" + transform.position);
            Debug.DrawLine( center.position, hit.point,Color.green);
            Pointer_to_Mouse.y = 0;
            player_ray.origin = transform.position;
            player_ray.direction = -transform.up*1000.0f;
            if (Physics.Raycast(player_ray, out player_hit, RAY_LENGTH, floorMask))
            {
                Debug.DrawLine(transform.position, hit.point, Color.magenta);

            }

            Quaternion new_rotation = Quaternion.LookRotation(Pointer_to_Mouse);
            // Do something with the object that was hit by the raycast.
            playerRigidbody.MoveRotation(new_rotation);
        }
        else
        {
            Debug.Log("no hit");
        }


    }
}